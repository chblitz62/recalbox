Drop here your EmulationStation event-driven scripts.

For more information, see: https://wiki.recalbox.com/fr/advanced-usage/scripts-on-emulationstation-events
