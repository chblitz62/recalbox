//
// Created by bkg2k on 07/03/24.
//
#pragma once

#include <utils/storage/HashMap.h>
#include <games/classifications/Regions.h>
#include <games/classifications/Genres.h>
#include <resources/TextureResource.h>
#include <memory>

class PictogramCaches
{
  public:
    /*!
     * @brief Get texture for the given region
     * @param region region to get flag texture from
     * @param height Target height for rasterization
     * @return Flag texture (or empty texture if the flag resource does not exist)
     */
    std::shared_ptr<TextureResource>& GetFlag(Regions::GameRegions region, int height);

    /*!
     * @brief Get texture for the given genre
     * @param genre genre to get texture from
     * @param height Target height for rasterization
     * @return Genre texture (or empty texture if the genre resource does not exist)
     */
    std::shared_ptr<TextureResource>& GetGenre(GameGenres genres, int height);

    /*!
     * @brief Get texture for the given players
     * @param playermin player min to get best texture from
     * @param playermax player max to get best texture from
     * @param height Target height for rasterization
     * @return Player texture (or empty texture if the player resource does not exist)
     */
    std::shared_ptr<TextureResource>& GetPlayers(int playermin, int playermax, int height);

    /*!
     * @brief Get support left part
     * @param height Target height for rasterization
     * @return Left part texture
     */
    std::shared_ptr<TextureResource>& GetSupportLeftPart(int height);

    /*!
     * @brief Get support right part
     * @param height Target height for rasterization
     * @return right part texture
     */
    std::shared_ptr<TextureResource>& GetSupportRightPart(int height);

    /*!
     * @brief Get support separator
     * @param height Target height for rasterization
     * @return separator texture
     */
    std::shared_ptr<TextureResource>& GetSupportSeparator(int height);

    /*!
     * @brief Get support side (A-B only, passing "none" returns an empty texture)
     * @param height Target height for rasterization
     * @return support side texture
     */
    std::shared_ptr<TextureResource>& GetSupportSide(SupportSides side, int height);

    /*!
     * @brief Get support number (1-20, out-of-range values returns empty textures)
     * @param height Target height for rasterization
     * @return support side texture
     */
    std::shared_ptr<TextureResource>& GetSupportNumber(int number, int height);

    //! Clean all flags to free memory
    void Clear()
    {
      mRegionToTextures.clear();
      mGenreToTextures.clear();
      mPlayersToTextures.clear();
      mSupportNumberToTextures.clear();
      mSupportSideToTextures.clear();
      mSupportLeft = nullptr;
      mSupportRight = nullptr;
      mSupportSeparator = nullptr;
    }

  private:
    //! region to flag texture
    HashMap<Regions::GameRegions, std::shared_ptr<TextureResource>> mRegionToTextures;
    //! GameGenre to genre texture
    HashMap<GameGenres, std::shared_ptr<TextureResource>> mGenreToTextures;
    //! GameGenre to genre texture
    HashMap<int, std::shared_ptr<TextureResource>> mPlayersToTextures;
    //! Support size textures
    HashMap<SupportSides, std::shared_ptr<TextureResource>> mSupportSideToTextures;
    //! Support number textures
    HashMap<int, std::shared_ptr<TextureResource>> mSupportNumberToTextures;
    //! Support left part
    std::shared_ptr<TextureResource> mSupportLeft;
    //! Support right part
    std::shared_ptr<TextureResource> mSupportRight;
    //! Support separator
    std::shared_ptr<TextureResource> mSupportSeparator;
    //! Hashmap locker
    Mutex mLocker;
};
