//
// Created by bkg2k on 21/08/24.
//

#include <guis/menus/base/ItemBase.h>
#include <guis/menus/base/IMenuInterface.h>
#include <systems/SystemData.h>

bool ItemBase::CalibrateLeftAndRightParts(int leftWidth, int& rightDynamicWidth, int rightStaticWidth)
{
  // Adjust width
  int half = mDataProvider.ItemWidth() / 2;
  if (rightDynamicWidth + rightStaticWidth + leftWidth > mDataProvider.ItemWidth())
  {
    // Reduce right option to make room for the label
    if (leftWidth < half) { rightDynamicWidth = half - rightStaticWidth; return true; }
    // Keep option space and let the label scroll by itself
    else if (rightDynamicWidth + rightStaticWidth < half) return false;
    // Both are bigger than half the space. Reduce the right part to the right half and let the label scroll in the left half
    else { rightDynamicWidth = half - rightStaticWidth; return true; }
  }
  else
  {
    // Clamp dynamic part to half the total item width
    if (rightDynamicWidth + rightStaticWidth > half) { rightDynamicWidth = half - rightStaticWidth; return true; }
  }
  return false;
}

ItemBase& ItemBase::ReplaceParameters()
{
  static String systemName("{SYSTEM.NAME}");
  static String gameName("{GAME.NAME}");
  static String index("{INDEX}");
  static String unknownSystem("<No system available>");
  static String unknownGame("<No game available>");
  mLabel.Replace(systemName, Context().HasSystem() ? Context().System()->Descriptor().FullName() : unknownSystem)
        .Replace(gameName, Context().HasGame() ? Context().Game()->Name() : unknownGame)
        .Replace(index, String(Context().Index()));
  mHelp.Replace(systemName, Context().HasSystem() ? Context().System()->Descriptor().FullName() : unknownSystem)
       .Replace(gameName, Context().HasGame() ? Context().Game()->Metadata().Name() : unknownGame)
       .Replace(index, String(Context().Index()));
  mHelpUnselectable.Replace(systemName, Context().HasSystem() ? Context().System()->Descriptor().FullName() : unknownSystem)
                   .Replace(gameName, Context().HasGame() ? Context().Game()->Name() : unknownGame)
                   .Replace(index, String(Context().Index()));
  mLabel.UpperCaseUTF8();
  //mHelp.UpperCaseUTF8();
  //mHelpUnselectable.UpperCaseUTF8();
  return *this;
}

ItemBase& ItemBase::ReplaceParameters(const String& parameter1, bool uppercase)
{
  mLabel = (_F(mLabel) / parameter1)();
  mHelp = (_F(mHelp) / parameter1)();
  mHelpUnselectable = (_F(mHelpUnselectable) / parameter1)();
  if (uppercase)
  {
    mLabel.UpperCaseUTF8();
    //mHelp.UpperCaseUTF8();
    //mHelpUnselectable.UpperCaseUTF8();
  }
  return *this;
}

ItemBase& ItemBase::ReplaceParameters(const String& parameter1, const String& parameter2, bool uppercase)
{
  mLabel = (_F(mLabel) / parameter1 / parameter2)();
  mHelp = (_F(mHelp) / parameter1 / parameter2)();
  mHelpUnselectable = (_F(mHelpUnselectable) / parameter1 / parameter2)();
  if (uppercase)
  {
    mLabel.UpperCaseUTF8();
    //mHelp.UpperCaseUTF8();
    //mHelpUnselectable.UpperCaseUTF8();
  }
  return *this;
}

ItemBase& ItemBase::ReplaceParameters(const String& parameter1, const String& parameter2, const String& parameter3, bool uppercase)
{
  mLabel = (_F(mLabel) / parameter1 / parameter2 / parameter3)();
  mHelp = (_F(mHelp) / parameter1 / parameter2 / parameter3)();
  mHelpUnselectable = (_F(mHelpUnselectable) / parameter1 / parameter2 / parameter3)();
  if (uppercase)
  {
    mLabel.UpperCaseUTF8();
    //mHelp.UpperCaseUTF8();
    //mHelpUnselectable.UpperCaseUTF8();
  }
  return *this;
}
