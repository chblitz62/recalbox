//
// Created by bkg2k on 07/02/25.
//
#pragma once

#include "MenuPersistantData.h"
#include "CrtConf.h"
#include "guis/menus/base/SelectorEntry.h"
#include "hardware/devices/storage/StorageDevices.h"
#include "scraping/scrapers/ScrapingMethod.h"
#include "themes/ThemeSpec.h"
#include "themes/ThemeData.h"
#include "hardware/crt/ICrtInterface.h"
#include "hardware/Overclocking.h"
#include "ResolutionAdapter.h"
#include "systems/SystemManager.h"

class MenuDataProvider
{
  public:
    MenuDataProvider(MenuPersistantData& persistantData, SystemManager& systemManager)
      : mSystemManager(systemManager)
      , mPersistantData(persistantData)
      , mConf(RecalboxConf::Instance())
      , mCrtConf(CrtConf::Instance())
    {
    }

    /*
     * Data getter
     */

    //! Get Storage List
    static SelectorEntry<StorageDevices::Device>::List GetStorageEntries(int& currentIndex);
    //! Get Culture List
    static SelectorEntry<String>::List GetCultureEntries();
    //! Get Keyboard List
    static SelectorEntry<String>::List GetKeyboardEntries();
    //! Get TimeZone List
    static SelectorEntry<String>::List GetTimeZones();
    //! Get resolution list for Kodi
    SelectorEntry<String>::List GetKodiResolutionsEntries();
    //! Get available scraping methods
    static SelectorEntry<ScrapingMethod>::List GetScrapingMethods();
    //! Get scrapable system list
    SelectorEntry<SystemData*>::List GetScrapableSystemsEntries();
    //! Get available scrappers
    static SelectorEntry<ScraperType>::List GetScrapersEntries();
    //! Get name retrieval methods when scraping
    static SelectorEntry<ScraperNameOptions>::List GetScraperNameOptionsEntries();
    //! Get available scraped image types
    static SelectorEntry<ScreenScraperEnums::ScreenScraperImageType>::List GetScraperImagesEntries();
    //! Get available scraped thumbnail types
    static SelectorEntry<ScreenScraperEnums::ScreenScraperImageType>::List GetScraperThumbnailsEntries();
    //! Get available scraped video types
    static SelectorEntry<ScreenScraperEnums::ScreenScraperVideoType>::List GetScraperVideosEntries();
    //! Get scraped region priority
    static SelectorEntry<ScreenScraperEnums::ScreenScraperRegionPriority>::List GetScraperRegionOptionsEntries();
    //! Get favorite region when scrping
    static SelectorEntry<Regions::GameRegions>::List GetScraperRegionsEntries();
    //! Get available scraped languages
    static SelectorEntry<Languages>::List GetScraperLanguagesEntries();
    //! Get Softpatching option list
    static SelectorEntry<RecalboxConf::SoftPatching>::List& GetSoftpatchingEntries();
    //! Get Bameboy mod entries
    static SelectorEntry<String>::List& GetSuperGameBoyEntries();
    //! Get libretro ratios
    static SelectorEntry<String>::List GetRatioEntries();
    //! Get Shaders List
    static SelectorEntry<String>::List GetShadersEntries();
    //! Get shader sets List
    static SelectorEntry<String>::List& GetShaderPresetsEntries();
    //! Get Netplay relay server list
    static SelectorEntry<RecalboxConf::Relay>::List& GetMitmEntries();
    //! Get Update type List
    static SelectorEntry<RecalboxConf::UpdateType>::List GetUpdateTypeEntries();
    //! Get screensavers
    SelectorEntry<RecalboxConf::Screensaver>::List GetTypeEntries();
    //! Getstatic  all system list as string
    SelectorEntry<String>::List GetSystemListAsString();
    //! Get all available themes
    SelectorEntry<ThemeSpec>::List GetThemeEntries(WindowManager& window);
    //! Get current theme
    ThemeSpec GetCurrentTheme();
    //! Get default theme
    static ThemeSpec GetDefaultTheme();
    // Get theme transitions
    SelectorEntry<String>::List GetThemeTransitionEntries();
    // Get theme regions
    SelectorEntry<String>::List GetThemeRegionEntries();
    //! Get System sorting list
    static SelectorEntry<SystemSorting>::List& GetSystemSortingEntries();
    //! Get hidable system list
    SelectorEntry<SystemData*>::List GetHidableSystems();
    //! Get manufacturer list
    static SelectorEntry<String>::List GetManufacturersVirtualEntries();
    //! Boot system list
    SelectorEntry<String>::List GetBootSystemEntries();
    //! Game genre list for multiple selection
    SelectorEntry<GameGenres>::List GetMultiGenreEntries();
    //! Game genre list for single static selection
    static SelectorEntry<GameGenres>::List GetSingleGenreEntries();
    //! Get global resolution list
    SelectorEntry<String>::List GetGlobalResolutionEntries();
    //! Get frontend resolution list
    SelectorEntry<String>::List GetResolutionEntries();
    //! Get overcstatic static lock list
    SelectorEntry<Overclocking::Overclock>::List GetOverclockEntries();
    //! Get emulator list for the given system
    SelectorEntry<String>::List GetEmulatorEntries(const SystemData* system, String& emulatorAndCore, String& defaultEmulatorAndCore);
    //! Get RGstatic static static B Adapter
    SelectorEntry<CrtAdapterType>::List GetDacEntries();
    // Get Crt Frostatic ntend rstatic esolution
    SelectorEntry<String>::List GetEsResolutionEntries();
    //! Crt horizontal frequencies
    static const SelectorEntry<ICrtInterface::HorizontalFrequency>::List& GetHorizontalOutputFrequency();
    //! Get CRT's super resolution list
    static const SelectorEntry<String>::List& GetSuperRezEntries();
    //! Get CRT's scanline list
    static const SelectorEntry<CrtScanlines>::List& GetScanlinesEntries();
    //! Get Cstatic ases list
    SelectorEntry<String>::List GetCasesEntries();
    //! Get audio output list
    static SelectorEntry<String>::List GetAudioOutputEntries();
    //! Get audio mode list
    static const SelectorEntry<AudioMode>::List& GetAudioModeEntries();
    //! Get first game letters from the gamelist related to the given system
    static SelectorEntry<String::Unicode>::List GetLetterEntries(SystemData* system);
    //! Get decorations list
    SelectorEntry<RecalboxConf::GamelistDecoration>::List GetDecorationEntries(const SystemData& system);
    //! Get region list
    static SelectorEntry<Regions::GameRegions>::List GetRegionEntries(const SystemData& system);
    //! Get sort list according to the given system
    static SelectorEntry<FileSorts::Sorts>::List GetSortEntries(const SystemData& system);
    //! Get the emulator/core list for the given game, and return default emulator/core in emulatorAndCore
    static SelectorEntry<String>::List GetEmulatorEntries(FileData& game, String& emulatorAndCore);
    //! Get the list of available patches for the given game
    static SelectorEntry<Path>::List GetPatchEntries(FileData& game);
    //! Get game file list of the given game
    static SelectorEntry<Path>::List GetGameFilesToDelete(FileData& game);
    //! Get media file list associatd to the gstatic iven game
    SelectorEntry<Path>::List GetMediaFilesToDelete(FileData& game);
    //! Get extra file list associatd to the given game
    static SelectorEntry<Path>::List GetExtraFilesToDelete(FileData& game);
    //! Get Save file list associatd to the gstatic iven game
    static SelectorEntry<Path>::List GetSaveFilesToDelete(FileData& game);
    //! Get softpatch list for the gstatic iven game
    SelectorEntry<Path>::List GetPatchesEntries(FileData& game);
    //! Get list of netplay passwords, including "none"
    SelectorEntry<int>::List GetNetplayPasswords();

  private:
    //! SystemManager reference
    SystemManager& mSystemManager;

    //! Persistant data
    MenuPersistantData& mPersistantData;
    //! Reclabox configuration reference
    RecalboxConf& mConf;
    //! Crt configuration reference
    CrtConf& mCrtConf;

    //! Resolution Adapter
    ResolutionAdapter mResolutionAdapter;
};
