//
// Created by bkg2k on 12/12/23.
//

#include "ThemeManager.h"
#include "RootFolders.h"
#include "guis/GuiSyncWaiter.h"
#include "utils/locale/LocaleHelper.h"
#include "guis/GuiMsgBox.h"
#include <systems/SystemData.h>

ThemeManager::ThemeManager(IGlobalVariableResolver& globalResolver)
  : StaticLifeCycleControler<ThemeManager>("theme-manager")
  , mMain(mCache, nullptr, globalResolver)
  , mMenuCache(mMenu)
  , mWaiter(nullptr)
  , mGlobalResolver(globalResolver)
{
  ThemeSupport::InitializeStatics();
}

void ThemeManager::Initialize(WindowManager* window)
{
  DoThemeChange(window);
}

ThemeManager::~ThemeManager()
{
  for(auto& kv : mSystem)
    delete kv.second;
}

Path ThemeManager::GetThemeRootPath()
{
  // Check theme validity
  String theme = RecalboxConf::Instance().GetThemeFolder();
  ThemeList list = AvailableThemes();
  if (!list.contains(theme))
    RecalboxConf::Instance().SetThemeFolder(list.begin()->second.FilenameWithoutExtension());

  return list[RecalboxConf::Instance().GetThemeFolder()];
}

void ThemeManager::LoadMainTheme()
{
  // Locate main theme file
  Path root;
  mRootPath = GetThemeRootPath();
  if (root = mRootPath / sRootThemeFile; !root.Exists())
  {
    // Try locating in subfolders
    for(const Path& sub : mRootPath.GetDirectoryContent(true))
      if ((sub / sRootThemeFile).Exists())
      {
        root = sub;
        break;
      }
  }

  mMain.Reset();
  mMain.LoadMain(root);
  mMenu.Load(mMain);
}

void ThemeManager::LoadSystemTheme(const SystemData& system)
{
  Path systemPath;
  // Check root/<system>/theme.xml first
  if (systemPath = mRootPath / system.Descriptor().ThemeFolder() / sRootThemeFile; !systemPath.Exists())
    // Then check root/default/theme.xml
    if (systemPath = mRootPath / sDefaultSystemThemeFolder / sRootThemeFile; !systemPath.Exists())
      // Then check root/theme.xml
      systemPath = mRootPath / sRootThemeFile;

  ThemeData& systemTheme = CreateOrGetSystem(&system);
  systemTheme.Reset();
  systemTheme.LoadSystem(system.Descriptor().ThemeFolder(), systemPath);
}

const SystemData* ThemeManager::ThreadPoolRunJob(const SystemData*& feed)
{
  LoadSystemTheme(*feed);
  return feed;
}

void ThemeManager::DoThemeChange(WindowManager* window, bool force)
{
  DateTime start;
  Path newPath = GetThemeRootPath();
  if (mRootPath.IsEmpty()) { LOGT(LogInfo) << "[ThemeManager] Loading initial theme: " << newPath; }
  else if (newPath != mRootPath) { LOGT(LogInfo) << "[ThemeManager] Switching to new theme: " << newPath; }
  else { LOGT(LogInfo) << "[ThemeManager] Current theme options havre changed. Refreshing theme: " << newPath; }

  //! Clear cache if required
  bool update = (newPath == mRootPath);
  if (!update || force) mCache.Clear();

  if (!mRootPath.IsEmpty())
  {
    mWaiter = new GuiSyncWaiter(*window, update ? _("Updating current theme...") :
                               (_F(_("Loading new theme {0}")) / newPath.FilenameWithoutExtension()).ToString());
    mWaiter->Show();
    mWaitBarReference = DateTime();
  }

  // Reload Main themes
  LoadMainTheme();
  // Reload system themes
  if (!mSystem.empty())
  {
    ThreadPool<const SystemData*, const SystemData*> pool(this, "theme-loader", false);
    for (auto& kv: mSystem) pool.PushFeed(kv.first);
    pool.Run(-2, false);
  }
  DateTime start2;
  { LOGT(LogWarning) << "[ThemeManager] Load time: " << (start2 - start).ToMillisecondsString() << " ms"; }
  // Refresh
  NotifyThemeChanged(update);
  { LOGT(LogWarning) << "[ThemeManager] Refresh time: " << (DateTime() - start2).ToMillisecondsString() << " ms"; }

  if (mWaiter != nullptr)
  {
    mWaiter->Hide();
    delete mWaiter;
    mWaiter = nullptr;
  }
}

void ThemeManager::ThemeSwitchTick()
{
  if (mWaiter != nullptr)
    if (DateTime now; (now - mWaitBarReference).TotalMilliseconds() > 300)
    {
      mWaiter->Refresh();
      mWaitBarReference = now;
    }
}

void ThemeManager::NotifyThemeChanged(bool refreshOnly)
{
  for(IThemeSwitchable* switchable : mSwitchables)
    if (switchable != nullptr)
    {
      if (SystemData* system = switchable->SystemTheme(); system != nullptr)
        switchable->SwitchToTheme(CreateOrGetSystem(system), refreshOnly, this);
      else
        switchable->SwitchToTheme(mMain, refreshOnly, this);
    }
}

ThemeData& ThemeManager::CreateOrGetSystem(const SystemData* system)
{
  Mutex::AutoLock locker(mSystemLocker);
  ThemeData** theme = mSystem.try_get(system);
  if (theme == nullptr) theme = &(mSystem[system] = new ThemeData(mCache, system, mGlobalResolver));
  return **theme;
}

HashMap<String, Path> ThemeManager::AvailableThemes()
{
  HashMap<String, Path> sets;

  static Path paths[] =
    {
      RootFolders::TemplateRootFolder / "/system/.emulationstation/themes",
      RootFolders::DataRootFolder     / "/themes",
      RootFolders::DataRootFolder     / "/system/.emulationstation/themes"
    };

  for (const Path& master : paths)
  {
    Path::PathList list = master.GetDirectoryContent();
    for(const Path& path : list)
      if (path.IsDirectory() && (path / sRootThemeFile).Exists())
      {
        String name = path.FilenameWithoutExtension();
        while(sets.contains(name))
          if (name.Count() >= 3 && name[name.Count() - 2] == '#' && name[name.Count() - 3] == ' ') name[name.Count()-2]++;
          else name.Append(" #2");
        sets[name] = path;
      }
  }

  return sets;
}

bool ThemeManager::AnalyseAndSwitch(WindowManager& window, const Path& themePath, [[out]] String& name, bool switchTheme)
{
  // Current state
  bool tate = mGlobalResolver.IsTate();
  ThemeData::Compatibility currentCompatibility = (mGlobalResolver.HasJamma() ? ThemeData::Compatibility::Jamma : ThemeData::Compatibility::None) |
                                                  (mGlobalResolver.HasCrt() ? ThemeData::Compatibility::Crt : ThemeData::Compatibility::None) |
                                                  (mGlobalResolver.HasHDMI() ? ThemeData::Compatibility::Hdmi : ThemeData::Compatibility::None);
  ThemeData::Resolutions currentResolution = mGlobalResolver.IsQVGA() ? ThemeData::Resolutions::QVGA :
                                             mGlobalResolver.IsVGA() ? ThemeData::Resolutions::VGA :
                                             mGlobalResolver.IsHD() ? ThemeData::Resolutions::HD :
                                             ThemeData::Resolutions::FHD;

  // Fetched data
  int recalboxVersion = 0; // Unknown version
  int themeVersion = 0; // Unknown version
  ThemeData::Compatibility compatibility = ThemeData::Compatibility::Hdmi;
  ThemeData::Resolutions resolutions = ThemeData::Resolutions::HD | ThemeData::Resolutions::FHD;
  String displayableName;

  if (ThemeData::FetchCompatibility(themePath / ThemeManager::sRootThemeFile, compatibility, resolutions, displayableName, themeVersion, recalboxVersion))
  {
    bool compatible = ((currentCompatibility & compatibility) != 0) &&
                      ((currentResolution & resolutions) != 0) &&
                      (!tate || hasFlag(compatibility, ThemeData::Compatibility::Tate)) &&
                      (recalboxVersion <= sRecalboxMinimumCompatibilityVersion);
    if (switchTheme)
    {
      if (!compatible)
      {
        String modeIssue = (currentCompatibility & compatibility) == 0 ?
                           String("- ").Append(_("You display {0} is not in the list of this theme's supported displays:"))
                                       .Replace("{0}",ThemeData::GetDisplayList(currentCompatibility))
                                       .Append(' ').Append(ThemeData::GetDisplayList(compatibility)).Append(String::CRLF) : String::Empty;
        String resolutionIssue = (currentResolution & resolutions) == 0 ?
                                 String("- ").Append(_("You current resolution {0} is not in the list of this theme's supported resolutions:"))
                                             .Replace("{0}",ThemeData::GetResolutionList(currentResolution))
                                             .Append(' ').Append(ThemeData::GetResolutionList(resolutions)).Append(String::CRLF) : String::Empty;
        String tateIssue = tate && !hasFlag(compatibility, ThemeData::Compatibility::Tate) ?
                           String("- ").Append(_("You're in TATE mode and this theme does not seem to support TATE.")).Append(String::CRLF) : String::Empty;
        String message = _("This theme may have one or more compatibility issues with your current display:\n").Append(modeIssue).Append(resolutionIssue).Append(tateIssue).Append(String::CRLF).Append("Are you sure to activate this theme?");
        window.pushGui(new GuiMsgBox(window, message, _("YES"), [&window]{ ThemeManager::Instance().DoThemeChange(&window); }, _("NO"), {}));
      }
      else
      {
        DoThemeChange(&window);
      }
    }
    if (themeVersion != 0) displayableName.Append(" (").Append(themeVersion >> 8).Append('.').Append(themeVersion & 0xFF).Append(')');

    name = displayableName;
    return compatible;
  }

  name = "<not a theme>";
  return false;
}
